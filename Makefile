#!/usr/bin/env -S make -f
# harry -- text-mode audio file viewer
# Copyright (C) 2019,2020,2022  Claude Heiland-Allen <claude@mathr.co.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

C99FLAGS = -std=c99 -Wall -Wextra -pedantic
OFLAGS = -O3
LIBS = -lsndfile -lncurses -lm
SOURCE = -Wl,harry.c -Wl,Makefile -Wl,README.md -Wl,COPYING.md
EMBED = -Wl,--format=binary $(SOURCE) -Wl,--format=default
OMP ?= -fopenmp
SDL ?= -DWITH_SDL2 `sdl2-config --cflags --libs`

harry: harry.c Makefile README.md COPYING.md
	gcc $(C99FLAGS) $(OFLAGS) $(OMP) -o harry harry.c $(LIBS) $(EMBED) $(SDL)
