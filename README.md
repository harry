---
title: harry -- text-mode audio file viewer
author: Claude Heiland-Allen
date: 2022-12-01
---

# harry

text-mode audio file viewer

<https://mathr.co.uk/harry>

## source code

<https://code.mathr.co.uk/harry>

    git clone https://code.mathr.co.uk/harry.git

## dependencies

required

- gcc
- make
- libncurses-dev
- libsndfile-dev

optional

- libsdl2-dev

## build

with SDL audio backend and OpenMP parallism:

    make

without SDL:

    make SDL=

without OpenMP parallelism:

    make OMP=

## help

    harry -- text-mode audio file viewer
    Copyright (C) 2019,2020,2022  Claude Heiland-Allen
    License: GNU AGPLv3+

    Usage:
        harry [--audio <driver>] FILE
            open audio file for interactive viewing
            this harry is compiled with these audio drivers:
                null
                sdl (default)
        harry -?,-h,-H,--help
            this message
        harry -v,-V,--version
            output version string
        harry -S,--source
            output harry's source code to the current working directory
            files written: harry.c Makefile README.md COPYING.md

    Keys:
        ESC, Q         quit
        Left, Right    scroll through time
        Up, Down       zoom in and out
        Home           zoom out to fit whole file in view
        End            zoom in to single samples
        W              waveform display
        S              spectrogram display
        +, =, -, 1     adjust audio display volume
        9, 0           adjust audio output volume
        M              toggle mute
        Space, P       toggle playback
        [, ]           adjust playback speed
        Shift-L        toggle file looping
        l              configure A-B looping
        F              toggle playback cursor follow mode

## display

harry does mipmap reduction for more-correct display of resampled signals.

### waveforms

harry displays signal statistics per cell column, averaged over duration:

- `v`: minimum
- `^`: maximum
- `@`: average
- `~`: deviation
- `-`: zero

### spectrums

harry displays RMS energy per octave in rows, higher pitches on top,
averaged over duration:

- `.,+oO0@#`: quiet to loud

### information

A-B loop points are marked with `:`, the play cursor is marked with `|`.

At the bottom is a representation of the whole file: outside the view
is `[` and `]`, inside the view is `=`, loop points are marked `|:` and
`:|`.  The play cursor is marked `>`.

## sound

harry plays sound using SDL2, which has a PulseAudio backend.  You can
run harry remotely and forward the audio over an SSH tunnel to play from
a local audio device:

    localhost$ scp ~/.config/pulse/cookie remotehost:.config/pulse/cookie
    localhost$ ssh -R 24713:localhost:4713 remotehost
    remotehost$ export PULSE_SERVER="tcp:localhost:24713"
    remotehost$ harry audio.wav

The remote host does not need PulseAudio daemon running.

## bugs

- harry v4 eats a large amount of RAM, over 8 bytes per sample per channel, 44 MB/min.
- harry v2 ate a huge amount of RAM, over 20 bytes per sample per channel, 110 MB/min.
- harry v1 ate a vast amount of RAM, over 32 bytes per sample per channel, 176 MB/min.
- CD audio eats 2 bytes per sample per channel, 11 MB/min.

## changelog

1.  initial release

2.  reduced memory consumption to a factor of 5 / 8

    fix use of uninitialized memory

    close SDL audio device when done

    free memory before exit

3. spectrum rendering via Haar wavelets

4. reduced memory consumption to a factor of 2 / 5

## legal

    harry -- text-mode audio file viewer
    Copyright (C) 2019,2020,2022  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
