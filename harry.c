/*
harry -- text-mode audio file viewer
Copyright (C) 2019,2020,2022  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define _GNU_SOURCE
#define _POSIX_C_SOURCE 199309

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <getopt.h>
#include <ncurses.h>
#include <sndfile.h>

#ifdef WITH_SDL2
#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>
#endif

#define OCTAVES 11
#define FACTOR 1.0594630943592953
#define RSQRT2 0.7071067811865475
#define TWOPI  6.283185307179586

extern const char _binary_harry_c_start[], _binary_harry_c_end[];
extern const char _binary_Makefile_start[], _binary_Makefile_end[];
extern const char _binary_README_md_start[], _binary_README_md_end[];
extern const char _binary_COPYING_md_start[], _binary_COPYING_md_end[];

typedef struct
{
  uint8_t f;
} float8; // 1 bits sign, 4 bits exponent, 4 bits mantissa (leading 1 implicit)
#define FLOAT8BIAS 0xC

float8 f8(float x)
{
  uint32_t y;
  memcpy(&y, &x, 4);
  uint8_t sign = (!!(y & 0x80000000ul)) << 7;
  int exponent = ((y >> 23) & 0xFF) - 0x7F + FLOAT8BIAS;
  int mantissa = (y & 0x007ffffful) >> 20;
  if (isnan(x))
  {
    float8 nan = { sign | 0x7f };
    return nan;
  }
  else if (isinf(x) || exponent >= 0xf)
  {
    float8 inf = { sign | 0x78 };
    return inf;
  }
  else if (x == 0 || exponent < 0)
  {
    float8 zero = { sign };
    return zero;
  }
  else if (exponent == 0)
  {
    // FIXME handle denormals
    float8 zero = { sign };
    return zero;
  }
  else
  {
    uint8_t expo = exponent << 3;
    float8 normal = { sign | expo | mantissa };
    return normal;
  }
}

float f32(float8 x)
{
  uint32_t sign = !!(x.f & 0x80);
  uint32_t expo = (x.f & 0x78) >> 3;
  uint32_t mantissa = (x.f & 0x7);
  if (expo == 0)
  {
    // FIXME handle denormals
    return sign ? -0.0f : 0.0f;
  }
  else if (expo == 0xf)
  {
    if (mantissa) // nan
    {
      return sign ? -0.0f/0.0f : 0.0f/0.0f;
    }
    else // inf
    {
      return sign ? -1.0f/0.0f : 1.0f/0.0f;
    }
  }
  else
  {
    uint32_t y = (sign << 31) | ((expo - FLOAT8BIAS + 0x7F) << 23) | (mantissa << 20);// * 0x124924);
    float z;
    memcpy(&z, &y, 4);
    return z;
  }
}

int write_file(const char *name, const char *start, const char *end)
{
  printf("writing '%s'... ", name);
  fflush(stdout);
  FILE *file = fopen(name, "wxb");
  if (! file)
  {
    printf("FAILED\n");
    return 0;
  }
  int ok = (fwrite(start, end - start, 1, file) == 1);
  fclose(file);
  if (ok)
    printf("ok\n");
  else
    printf("FAILED\n");
  return ok;
}

struct waveform
{
  struct waveform *next;
  struct waveform *prev;
  double samplerate;
  int channels;
  ssize_t frames;
  union
  {
    struct
    {
      float *rms;
      float *avg;
      float *min;
      float *max;
    } f; // borrowed == 1
    struct
    {
      float8 *rms;
      float8 *avg;
      float8 *min;
      float8 *max;
    } f8; // borrowed == 0
  } u;
  int borrowed;
  int octaves;
  float8 *spectrum;
};

struct waveform_list
{
  struct waveform head;
  struct waveform tail;
};

void free_waveform(struct waveform *w)
{
  if (! w) return;
  if (w->next) w->next->prev = w->prev;
  if (w->prev) w->prev->next = w->next;
  w->next = 0;
  w->prev = 0;
  if (w->borrowed)
  {
    if (w->u.f.rms) free(w->u.f.rms);
    w->u.f.rms = 0;
    w->u.f.avg = 0;
    w->u.f.min = 0;
    w->u.f.max = 0;
  }
  else
  {
    if (w->u.f8.rms) free(w->u.f8.rms);
    if (w->u.f8.avg) free(w->u.f8.avg);
    if (w->u.f8.min) free(w->u.f8.min);
    if (w->u.f8.max) free(w->u.f8.max);
    w->u.f8.rms = 0;
    w->u.f8.avg = 0;
    w->u.f8.min = 0;
    w->u.f8.max = 0;
  }
  if (w->spectrum) free(w->spectrum);
  w->spectrum = 0;
  free(w);
}

void free_waveform_list(struct waveform_list *l)
{
  if (! l) return;
  while (l->head.next->next) free_waveform(l->head.next);
  free(l);
}

struct waveform *read_sound_file(const char *name)
{
  struct waveform *w = calloc(1, sizeof(*w));
  if (! w)
  {
    return 0;
  }
  SF_INFO info;
  memset(&info, 0, sizeof(info));
  SNDFILE *ifile = sf_open(name, SFM_READ, &info);
  if (! ifile)
  {
    free(w);
    return 0;
  }
  w->samplerate = info.samplerate;
  w->channels = info.channels;
  w->frames = info.frames;
  ssize_t bytes = sizeof(float) * (w->frames + (w->frames & 1)) * w->channels;
  w->u.f.rms = malloc(bytes);
  w->u.f.avg = w->u.f.rms;
  w->u.f.min = w->u.f.rms;
  w->u.f.max = w->u.f.rms;
  w->borrowed = 1;
  if (w->frames & 1)
    for (int channel = 0; channel < w->channels; ++channel)
      w->u.f.rms[w->frames * w->channels + channel] = 0;
  if (w->u.f.rms && w->frames == sf_readf_float(ifile, w->u.f.rms, w->frames))
  {
    sf_close(ifile);
    return w;
  }
  else
  {
    sf_close(ifile);
    free_waveform(w);
    return 0;
  }
}

struct waveform_list *singleton(struct waveform *base)
{
  if (! base)
  {
    return 0;
  }
  struct waveform_list *l = calloc(1, sizeof(*l));
  if (! l)
  {
    free_waveform(base);
    return 0;
  }
  l->head.next = base;
  l->tail.prev = base;
  base->next = &l->tail;
  base->prev = &l->head;
  return l;
}

struct waveform_list *build_mipmaps(struct waveform_list *l)
{
  if (! l) return 0;
  ssize_t level = 1;
  float window[1 << OCTAVES];
  for (ssize_t k = 0; k < 1 << OCTAVES; ++k)
  {
    window[k] = (1 - cos(TWOPI * k / (1 << OCTAVES)));
  }
  while (l->head.next->frames > 1)
  {
    struct waveform *u = l->head.next;
    struct waveform *v = calloc(1, sizeof(*v));
    if (! v)
    {
      free_waveform_list(l);
      return 0;
    }
    v->samplerate = u->samplerate / 2;
    v->channels = u->channels;
    v->frames = (u->frames + 1) / 2;
    ssize_t bytes = sizeof(float8) * (v->frames + (v->frames & 1)) * v->channels;
    v->u.f8.rms = malloc(bytes);
    v->u.f8.avg = malloc(bytes);
    v->u.f8.min = malloc(bytes);
    v->u.f8.max = malloc(bytes);
    if (level >= 8)
    {
      bytes *= OCTAVES;
      v->octaves = OCTAVES;
      v->spectrum = malloc(bytes);
    }
    if (v->u.f8.rms && v->u.f8.avg && v->u.f8.min && v->u.f8.max && (level < 8 || v->spectrum))
    {
      if (level == 8)
      {
        struct waveform *w = l->tail.prev;
        assert(w->borrowed);
        #pragma omp parallel for schedule(static)
        for (ssize_t i = 0; i < v->frames + (v->frames & 1); ++i)
        {
          ssize_t j0 = i << 8;
          for (ssize_t c = 0; c < w->channels; ++c)
          {
            float haar[2][1 << OCTAVES];
            memset(haar, 0, sizeof(haar));
            for ( ssize_t k = 0, j = j0
                ; k < 1 << OCTAVES && j < (w->frames + (w->frames & 1))
                ; ++k, ++j
                )
            {
              haar[0][k] = w->u.f.rms[j * w->channels + c] * window[k];
            }
            // compute Haar wavelet transform
            for (ssize_t length = (1 << OCTAVES) >> 1; length > 0; length >>= 1)
            {
              for (ssize_t i = 0; i < length; ++i)
              {
                float a = haar[0][2 * i + 0];
                float b = haar[0][2 * i + 1];
                float s = (a + b) * 0.5f;// * RSQRT2;
                float d = (a - b) * 0.5f;// * RSQRT2;
                haar[1][         i] = s;
                haar[1][length + i] = d;
              }
              for (int i = 0; i < 1 << OCTAVES; ++i)
              {
                haar[0][i] = haar[1][i];
              }
            }
            // compute energy per octave
            float E[OCTAVES + 1];
            memset(E, 0, sizeof(E));
            E[0] += fabsf(haar[0][0]); // DC
            for ( ssize_t octave = 1, length = 1
                ; length <= (1 << OCTAVES) >> 1
                ; octave += 1, length <<= 1
                )
            {
              double rms = 0;
              for (int i = 0; i < length; ++i)
              {
                double a = haar[0][length + i];
                rms += a * a;
              }
              rms /= length;
              rms = sqrt(fmax(rms, 0));
              E[octave] = rms;
            }
            // copy to spectrum
            ssize_t m = (i * v->channels + c) * v->octaves;
            for (ssize_t octave = 1; octave <= v->octaves; ++octave)
            {
              v->spectrum[m + (octave - 1)] = f8(E[octave]);
            }
          }
        }
      }
      #pragma omp parallel for schedule(static)
      for (ssize_t i = 0; i < v->frames + (v->frames & 1); ++i)
      {
        for (ssize_t c = 0; c < v->channels; ++c)
        {
          if (u->borrowed)
          {
            float mi = 1.0 / 0.0;
            float ma = -1.0 / 0.0;
            float av = 0;
            float s2 = 0;
            float s0 = 0;
            for (ssize_t j = 2 * i; j < 2 * i + 2 && j < u->frames; ++j)
            {
              ssize_t ix = j * u->channels + c;
              mi = fminf(mi, u->u.f.min[ix]);
              ma = fmaxf(ma, u->u.f.max[ix]);
              av = av + u->u.f.avg[ix];
              s2 = s2 + u->u.f.rms[ix] * u->u.f.rms[ix];
              s0 = s0 + 1;
            }
            ssize_t ix = i * v->channels + c;
            v->u.f8.min[ix] = f8(mi);
            v->u.f8.max[ix] = f8(ma);
            v->u.f8.avg[ix] = f8(av / s0);
            v->u.f8.rms[ix] = f8(sqrtf(s2 / s0));
          }
          else
          {
            float mi = 1.0 / 0.0;
            float ma = -1.0 / 0.0;
            float av = 0;
            float s2 = 0;
            float s0 = 0;
            for (ssize_t j = 2 * i; j < 2 * i + 2 && j < u->frames; ++j)
            {
              ssize_t ix = j * u->channels + c;
              mi = fminf(mi, f32(u->u.f8.min[ix]));
              ma = fmaxf(ma, f32(u->u.f8.max[ix]));
              av = av + f32(u->u.f8.avg[ix]);
              float rms = f32(u->u.f8.rms[ix]);
              s2 = s2 + rms * rms;
              s0 = s0 + 1;
            }
            ssize_t ix = i * v->channels + c;
            v->u.f8.min[ix] = f8(mi);
            v->u.f8.max[ix] = f8(ma);
            v->u.f8.avg[ix] = f8(av / s0);
            v->u.f8.rms[ix] = f8(sqrtf(s2 / s0));
          }
        }
      }
      if (level > 8)
      {
        #pragma omp parallel for schedule(static)
        for (ssize_t i = 0; i < v->frames + (v->frames & 1); ++i)
        {
          for (ssize_t c = 0; c < v->channels; ++c)
          {
            float spectrum[v->octaves];
            ssize_t ix = (i * v->channels + c) * v->octaves;
            for (ssize_t o = 0; o < v->octaves; ++o)
            {
              spectrum[o] = 0;
            }
            float s0 = 0;
            for (ssize_t j = 2 * i; j < 2 * i + 2 && j < u->frames; ++j)
            {
              ssize_t jx = (j * u->channels + c) * u->octaves;
              for (ssize_t o = 0; o < u->octaves; ++o)
              {
                spectrum[o] += f32(u->spectrum[jx + o]);
              }
              s0 = s0 + 1;
            }
            for (ssize_t o = 0; o < v->octaves; ++o)
            {
              v->spectrum[ix + o] = f8(spectrum[o] / s0);
            }
          }
        }
      }
    }
    else
    {
      free_waveform(v);
      free_waveform_list(l);
      return 0;
    }
    v->next = l->head.next;
    v->prev = &l->head;
    l->head.next->prev = v;
    l->head.next = v;
    level++;
  }
  return l;
}

struct cursor
{
  struct waveform *wave;
  double offset;
  double loggain;
  double logspeed;
};

int cursor_up(struct cursor *c, int width, int display_mode)
{
  (void) width;
  if (c->wave->next->next && (display_mode ? !!c->wave->next->spectrum : 1))
  {
    c->wave = c->wave->next;
    c->offset *= 2;
    if (c->offset > c->wave->frames)
      c->offset = c->wave->frames;
    return 1;
  }
  return 0;
}

void cursor_end(struct cursor *c, int width, int display_mode)
{
  while (cursor_up(c, width, display_mode))
    ;
}

int cursor_down(struct cursor *c, int width)
{
  if (c->wave->prev->prev && c->wave->prev->frames >= width / 4)
  {
    c->wave = c->wave->prev;
    c->offset /= 2;
    if (c->offset < 0)
      c->offset = 0;
    return c->wave->frames >= width / 2;
  }
  return 0;
}

void cursor_home(struct cursor *c, int width, int display_mode)
{
  cursor_up(c, width, display_mode);
  while (cursor_down(c, width))
    ;
}

void cursor_left(struct cursor *c, int width)
{
  c->offset -= width / 4.0;
  if (c->offset <= 0)
  {
    c->offset = 0;
  }
}

void cursor_right(struct cursor *c, int width)
{
  c->offset += width / 4.0;
  if (c->offset >= c->wave->frames)
  {
    c->offset = c->wave->frames;
  }
}

struct cursor *cursor_init(struct waveform_list *l, int width, int display_mode)
{
  if (width < 1) return 0;
  struct cursor *c = calloc(1, sizeof(*c));
  if (! c) return 0;
  c->wave = l->tail.prev;
  c->offset = 0;
  c->loggain = 0;
  c->logspeed = 0;
  cursor_home(c, width, display_mode);
  return c;
}

void printw_time(double ms)
{
  int milliseconds = floor(fmod(ms, 1000.0));
  int seconds = floor(fmod(ms, 1000.0 * 60) / 1000.0);
  int minutes = floor(fmod(ms, 1000.0 * 60 * 60) / (1000 * 60));
  int hours = floor(fmod(ms, 1000.0 * 60 * 60 * 24) / (1000 * 60 * 60));
  int days = floor(ms / (1000.0 * 60 * 60 * 24));
  printw
    ( "%d:%02d:%02d:%02d.%03d"
    , days, hours, minutes, seconds, milliseconds
    );
}

struct audio
{
  struct waveform_list *waveforms;
  double offset;
  int loggain;
  double gain;
  double increment;
  int floor_log2_increment;
  double fract_log2_increment;
  double factor;
  int playing;
  int looping;
  int mute;
  int ab;
  double loop_start;
  double loop_end;
  int follow;
};

void display_waveform(struct cursor *c, const char *name, struct audio *a)
{
  static int seed = 147;
  srand(seed++);
  int row = 0, col = 0;
  getmaxyx(stdscr, row, col);
  int n = c->wave->channels;
  int y0 = (row - 1) % (n * 2);
  row -= y0 + 1;
  float gain = pow(FACTOR, c->loggain);
  int first = 1;
  double start = -1.0 / 0.0;
  double end = 1.0 / 0.0;
  for (int i = 0; i <= col; ++i)
  {
    double k = c->offset + i - col / 2;
    if (0 <= k && k < c->wave->frames)
    {
      if (first)
      {
        start = k;
        first = 0;
      }
      end = k;
      for (int channel = 0; channel < n; ++channel)
      {
        ssize_t ix = ((ssize_t)(floor(k))) * n + channel;
        float avg, rms, min, max;
        if (c->wave->borrowed)
        {
          avg = c->wave->u.f.avg[ix];
          rms = c->wave->u.f.rms[ix];
          min = c->wave->u.f.min[ix];
          max = c->wave->u.f.max[ix];
        }
        else
        {
          avg = f32(c->wave->u.f8.avg[ix]);
          rms = f32(c->wave->u.f8.rms[ix]);
          min = f32(c->wave->u.f8.min[ix]);
          max = f32(c->wave->u.f8.max[ix]);
        }
        avg *= gain;
        rms *= gain;
        min *= gain;
        max *= gain;
        float dev = sqrtf(fmaxf(0, rms * rms - avg * avg));
        if (min > 0 || max < 0) rms = 0;
        float ys[] = { min, max, avg - dev, avg + dev, avg, 0 };
        for (int l = 0; l < 6; ++l)
        {
          float y = -ys[l];
          y = fminf(y, 1);
          y = fmaxf(y, -1);
          // -1 .. 1
          y /= 2;
          y += 0.5;
          // 0 .. 1
          y += channel;
          // 0 .. channels
          y /= n;
          y *= row;
          // dither
          y += rand() / (double) RAND_MAX - 0.5;
          // 0 .. row
          y = roundf(y);
          if (y < 0) y = 0;
          if (y > row - 1) y = row - 1;
          ys[l] = y + y0;
        }
        min = fminf(ys[0], ys[1]);
        max = fmaxf(ys[0], ys[1]);
        float devi = fminf(ys[2], ys[3]);
        float deva = fmaxf(ys[2], ys[3]);
        avg = ys[4];
        mvaddch(min, i, '^' | A_DIM);
        for (int j = min + 1; j < max; ++j)
          mvaddch(j, i, '+' | A_DIM);
        mvaddch(max, i, 'v' | A_DIM);
        if (min <= devi && deva <= max)
        {
          mvaddch(devi, i, '~' | A_BOLD);
          for (int j = devi + 1; j < deva; ++j)
            mvaddch(j, i, '*' | A_BOLD);
          mvaddch(deva, i, '~' | A_BOLD);
        }
        mvaddch(ys[4], i, '@' | A_BOLD);
        if (ys[4] != ys[5])
          mvaddch(ys[5], i, '-');
      }
    }
    if (a->follow && i == col / 2)
      for (int j = 0; j < row; ++j)
        mvaddch(y0 + j, i, '|');
  }
  if (a->ab > 0)
  {
    double k = a->loop_start * c->wave->frames / a->waveforms->tail.prev->frames;
    double i = k + col / 2 - c->offset;
    if (0 <= i && i < col)
    {
      int ii = floor(i);
      for (int j = 0; j < row; ++j)
        mvaddch(y0 + j, ii, ':' | A_BOLD);
    }
  }
  if (a->ab > 1)
  {
    double k = a->loop_end * c->wave->frames / a->waveforms->tail.prev->frames;
    double i = k + col / 2 - c->offset;
    if (-1 < i && i <= col - 1)
    {
      int ii = ceil(i);
      for (int j = 0; j < row; ++j)
        mvaddch(y0 + j, ii, ':' | A_BOLD);
    }
  }
  if (! a->follow)
  {
    double k = a->offset * c->wave->frames / a->waveforms->tail.prev->frames;
    double i = k + col / 2 - c->offset;
    if (-0.5 <= i && i < col + 0.5)
    {
      int ii = round(i);
      for (int j = 0; j < row; ++j)
        mvaddch(y0 + j, ii, '|' | A_BOLD);
    }
  }
  mvprintw(0, 0, "file ");
  printw_time(c->wave->frames * 1000.0 / c->wave->samplerate);
  printw(" %s", name);
  mvprintw(1, 0, "view ");
  printw_time(col * 1000.0 / c->wave->samplerate);
  printw
    ( " gain %g | speed %g"
    , pow(FACTOR, c->loggain)
    , pow(FACTOR, c->logspeed)
    );
  if (a->ab == 1) printw(" | A");
  if (a->ab == 2) printw(" | A-B");
  if (a->looping) printw(" | loop");
  if (a->playing) printw(" | play");
  if (a->mute) printw(" | mute");
  mvprintw(2, 0, "at   ");
  printw_time(c->offset * 1000.0 / c->wave->samplerate);
  {
    int col0 = floor(col * fmin(fmax(0, start / c->wave->frames), 1));
    int col1 = ceil (col * fmin(fmax(0, end   / c->wave->frames), 1));
    for (int i = 0; i < col0; ++i)
      mvaddch(y0 + row, i, '[' | A_DIM);
    for (int i = col0; i < col1; ++i)
      mvaddch(y0 + row, i, '=');
    for (int i = col1; i < col; ++i)
      mvaddch(y0 + row, i, ']' | A_DIM);
  }
  {
    double len = a->waveforms->tail.prev->frames;
    int start = floor(col * a->loop_start / len);
    int end   = floor(col * a->loop_end   / len);
    if (a->ab > 0)
    {
      mvaddch(y0 + row, start, '|' | A_BOLD);
      mvaddch(y0 + row, start + 1, ':' | A_BOLD);
    }
    if (a->ab > 1)
    {
      mvaddch(y0 + row, end, ':' | A_BOLD);
      mvaddch(y0 + row, end + 1, '|' | A_BOLD);
    }
    mvaddch(y0 + row , floor(col * a->offset / len), '>' | A_BOLD);
  }
}

void display_spectrum(struct cursor *c, const char *name, struct audio *a)
{
  static int seed = 147;
  srand(seed++);
  int row = 0, col = 0;
  getmaxyx(stdscr, row, col);
  int n = c->wave->channels;
  int y0 = 4;
  row -= y0 + 1;
  float gain = pow(FACTOR, c->loggain);
  int first = 1;
  double start = -1.0 / 0.0;
  double end = 1.0 / 0.0;
  for (int i = 0; i <= col; ++i)
  {
    int j = y0;
    double k = c->offset + i - col / 2;
    if (0 <= k && k < c->wave->frames)
    {
      if (first)
      {
        start = k;
        first = 0;
      }
      end = k;
      const char CHARS[] = { '.', ',', '+', 'o', 'O', '0', '@', '#' };
      for (int channel = 0; channel < n; ++channel)
      {
        ssize_t ix = (((ssize_t)(floor(k))) * n + channel) * c->wave->octaves;
        for (int octave = c->wave->octaves - 1; octave >= 0; --octave)
        {
          float rms = f32(c->wave->spectrum[ix + octave]) * gain * c->wave->octaves;
          int ch = roundf(rms * sizeof(CHARS) + rand() / (double) RAND_MAX - 0.5);
          if (ch < 0) ch = 0;
          if ((size_t)ch > sizeof(CHARS) - 1) ch = sizeof(CHARS) - 1;
          mvaddch(j++, i, CHARS[ch] | (ch < 2 ? A_DIM : ch >= 6 ? A_BOLD : 0));
        }
        j++;
      }
    }
    if (a->follow && i == col / 2)
      for (int j = 0; j < row; ++j)
        mvaddch(y0 + j, i, '|');
  }
  if (a->ab > 0)
  {
    double k = a->loop_start * c->wave->frames / a->waveforms->tail.prev->frames;
    double i = k + col / 2 - c->offset;
    if (0 <= i && i < col)
    {
      int ii = floor(i);
      for (int j = 0; j < row; ++j)
        mvaddch(y0 + j, ii, ':' | A_BOLD);
    }
  }
  if (a->ab > 1)
  {
    double k = a->loop_end * c->wave->frames / a->waveforms->tail.prev->frames;
    double i = k + col / 2 - c->offset;
    if (-1 < i && i <= col - 1)
    {
      int ii = ceil(i);
      for (int j = 0; j < row; ++j)
        mvaddch(y0 + j, ii, ':' | A_BOLD);
    }
  }
  if (! a->follow)
  {
    double k = a->offset * c->wave->frames / a->waveforms->tail.prev->frames;
    double i = k + col / 2 - c->offset;
    if (-0.5 <= i && i < col + 0.5)
    {
      int ii = round(i);
      for (int j = 0; j < row; ++j)
        mvaddch(y0 + j, ii, '|' | A_BOLD);
    }
  }
  mvprintw(0, 0, "file ");
  printw_time(c->wave->frames * 1000.0 / c->wave->samplerate);
  printw(" %s", name);
  mvprintw(1, 0, "view ");
  printw_time(col * 1000.0 / c->wave->samplerate);
  printw
    ( " gain %g | speed %g"
    , pow(FACTOR, c->loggain)
    , pow(FACTOR, c->logspeed)
    );
  if (a->ab == 1) printw(" | A");
  if (a->ab == 2) printw(" | A-B");
  if (a->looping) printw(" | loop");
  if (a->playing) printw(" | play");
  if (a->mute) printw(" | mute");
  mvprintw(2, 0, "at   ");
  printw_time(c->offset * 1000.0 / c->wave->samplerate);
  {
    int col0 = floor(col * fmin(fmax(0, start / c->wave->frames), 1));
    int col1 = ceil (col * fmin(fmax(0, end   / c->wave->frames), 1));
    for (int i = 0; i < col0; ++i)
      mvaddch(y0 + row, i, '[' | A_DIM);
    for (int i = col0; i < col1; ++i)
      mvaddch(y0 + row, i, '=');
    for (int i = col1; i < col; ++i)
      mvaddch(y0 + row, i, ']' | A_DIM);
  }
  {
    double len = a->waveforms->tail.prev->frames;
    int start = floor(col * a->loop_start / len);
    int end   = floor(col * a->loop_end   / len);
    if (a->ab > 0)
    {
      mvaddch(y0 + row, start, '|' | A_BOLD);
      mvaddch(y0 + row, start + 1, ':' | A_BOLD);
    }
    if (a->ab > 1)
    {
      mvaddch(y0 + row, end, ':' | A_BOLD);
      mvaddch(y0 + row, end + 1, '|' | A_BOLD);
    }
    mvaddch(y0 + row , floor(col * a->offset / len), '>' | A_BOLD);
  }
}

void exit_cb(void)
{
  endwin();
}

void audio_speed(struct audio *a, double speed)
{
  a->increment = speed / a->factor;
  double l = log2(a->increment);
  a->floor_log2_increment = floor(l);
  a->fract_log2_increment = l - floor(l);
}

#ifdef WITH_SDL2

static inline float tabread4f32
  ( const float *buffer, ssize_t l, ssize_t channels
  , ssize_t channel, double d
  )
{
  ssize_t d1 = floor(d);
  ssize_t d0 = d1 - 1;
  ssize_t d2 = d1 + 1;
  ssize_t d3 = d1 + 2;
  double t = d - d1;
  d0 = (0 <= d0 && d0 < l) ? d0 : 0;
  d1 = (0 <= d1 && d1 < l) ? d1 : d0;
  d2 = (0 <= d2 && d2 < l) ? d2 : d1;
  d3 = (0 <= d3 && d3 < l) ? d3 : d2;
  double y0 = buffer[channels * d0 + channel];
  double y1 = buffer[channels * d1 + channel];
  double y2 = buffer[channels * d2 + channel];
  double y3 = buffer[channels * d3 + channel];
  double a0 = -t*t*t + 2*t*t - t;
  double a1 = 3*t*t*t - 5*t*t + 2;
  double a2 = -3*t*t*t + 4*t*t + t;
  double a3 = t*t*t - t*t;
  return (a0 * y0 + a1 * y1 + a2 * y2 + a3 * y3) / 2;
}

static inline float tabread4f8
  ( const float8 *buffer, ssize_t l, ssize_t channels
  , ssize_t channel, double d
  )
{
  ssize_t d1 = floor(d);
  ssize_t d0 = d1 - 1;
  ssize_t d2 = d1 + 1;
  ssize_t d3 = d1 + 2;
  double t = d - d1;
  d0 = (0 <= d0 && d0 < l) ? d0 : 0;
  d1 = (0 <= d1 && d1 < l) ? d1 : d0;
  d2 = (0 <= d2 && d2 < l) ? d2 : d1;
  d3 = (0 <= d3 && d3 < l) ? d3 : d2;
  double y0 = f32(buffer[channels * d0 + channel]);
  double y1 = f32(buffer[channels * d1 + channel]);
  double y2 = f32(buffer[channels * d2 + channel]);
  double y3 = f32(buffer[channels * d3 + channel]);
  double a0 = -t*t*t + 2*t*t - t;
  double a1 = 3*t*t*t - 5*t*t + 2;
  double a2 = -3*t*t*t + 4*t*t + t;
  double a3 = t*t*t - t*t;
  return (a0 * y0 + a1 * y1 + a2 * y2 + a3 * y3) / 2;
}

static inline void audio1(struct audio *a, float *out, ssize_t channels)
{
  if (a->increment != 0)
  {
    int l = a->floor_log2_increment;
    float f = a->fract_log2_increment;
    int l1 = l + 1;
    float f1 = 1 - f;
    const struct waveform *base = a->waveforms->tail.prev;
    for (int i = 0; i < l && base->prev->prev; ++i)
      base = base->prev;
    double base_offset = a->offset / pow(2, fmax(0, l));
    const struct waveform *next = base->prev;
    double next_offset = a->offset / pow(2, l1);
    if (l + f > 0 && next->prev)
    {
      for (int channel = 0; channel < channels; ++channel)
        out[channel] = (! a->mute) * a->gain *
          ( f1 * (base->borrowed ? tabread4f32
            ( base->u.f.avg, base->frames, base->channels
            , channel, base_offset
            ) : tabread4f8
            ( base->u.f8.avg, base->frames, base->channels
            , channel, base_offset
            ))
          + f  * (next->borrowed ? tabread4f32
            ( next->u.f.avg, next->frames, next->channels
            , channel, next_offset
            ) : tabread4f8
            ( next->u.f8.avg, next->frames, next->channels
            , channel, next_offset
            ))
          );
    }
    else
    {
      for (int channel = 0; channel < channels; ++channel)
        out[channel] = (! a->mute) * a->gain * (base->borrowed ? tabread4f32
          ( base->u.f.avg, base->frames, base->channels
          , channel, base_offset
          ) : tabread4f8
          ( base->u.f8.avg, base->frames, base->channels
          , channel, base_offset
          ));
    }
    double inc = a->increment;
    a->offset += inc;
    if (a->ab == 2)
    {
      double len = a->waveforms->tail.prev->frames;
      double start = a->loop_start;
      double end = a->loop_end;
      if (end < start) end += len;
      if (a->offset < start) a->offset += len;
      a->offset = fmod(a->offset - start, end - start) + start;
      a->offset = fmod(a->offset, len);
    }
    else if (a->offset >= a->waveforms->tail.prev->frames)
    {
      if (a->looping)
      {
        a->offset = fmod(a->offset, a->waveforms->tail.prev->frames);
      }
      else
      {
        a->playing = 0;
        audio_speed(a, 0);
      }
    }
  }
  else
  {
    for (ssize_t c = 0; c < channels; ++c)
    {
      out[c] = 0;
    }
  }
}

void audio_cb(void *userdata, Uint8 *stream, int len)
{
  struct audio *a = userdata;
  ssize_t c = a->waveforms->tail.prev->channels;
  float *b = (float *) stream;
  ssize_t m = len / sizeof(float) / c;
  ssize_t k = 0;
  for (ssize_t i = 0; i < m; ++i)
  {
    float out[c];
    audio1(a, out, c);
    for (int j = 0; j < c; ++j)
    {
      b[k++] = out[j];
    }
  }
}

#endif

enum audio_device
  { audio_null
#ifdef WITH_SDL2
  , audio_sdl
#endif
  };

int main(int argc, char **argv)
{
  enum audio_device adev = audio_null;
  (void) adev;
#ifdef WITH_SDL2
  adev = audio_sdl;
#endif
  while (1)
  {
    int option_index = 0;
    static struct option long_options[] =
      { { "audio",   required_argument, 0, 'a' }
      , { "help",    no_argument,       0, 'h' }
      , { "version", no_argument,       0, 'v' }
      , { "source",  no_argument,       0, 'S' }
      , { 0,         0,                 0,  0  }
      };
    int opt = getopt_long(argc, argv, "a:hH?vVS", long_options, &option_index);
    if (opt == -1) break;
    switch (opt)
    {
      case 'a':
        if (0 == strcmp(optarg, "null")) adev = audio_null;
#ifdef WITH_SDL2
        else
        if (0 == strcmp(optarg, "sdl")) adev = audio_sdl;
#endif
        else
        {
          fprintf(stderr, "%s: error: unknown audio driver '%s'\n", argv[0], optarg);
          return 1;
        }
        break;
      case 'h':
      case 'H':
      case '?':
        printf(
          "harry -- text-mode audio file viewer\n"
          "Copyright (C) 2019,2020,2022  Claude Heiland-Allen\n"
          "License: GNU AGPLv3+\n"
          "\n"
          "Usage:\n"
          "    %s [--audio <driver>] FILE\n"
          "        open audio file for interactive viewing\n"
          "        this harry is compiled with these audio drivers:\n"
#ifdef WITH_SDL2
          "            null\n"
          "            sdl (default)\n"
#else
          "            null (default)\n"
#endif
          "    %s -?,-h,-H,--help\n"
          "        this message\n"
          "    %s -v,-V,--version\n"
          "        output version string\n"
          "    %s -S,--source\n"
          "        output %s's source code to the current working directory\n"
          "        files written: harry.c Makefile README.md COPYING.md\n"
          "\n"
          "Keys:\n"
          "    ESC, Q         quit\n"
          "    Left, Right    scroll through time\n"
          "    Up, Down       zoom in and out\n"
          "    Home           zoom out to fit whole file in view\n"
          "    End            zoom in to single samples\n"
          "    W              waveform display\n"
          "    S              spectrogram display\n"
          "    +, =, -, 1     adjust audio display volume\n"
          "    9, 0           adjust audio output volume\n"
          "    M              toggle mute\n"
          "    Space, P       toggle playback\n"
          "    [, ]           adjust playback speed\n"
          "    Shift-L        toggle looping\n"
          "    l              configure A-B looping\n"
          "    F              toggle playback cursor follow mode\n"
          , argv[0], argv[0], argv[0], argv[0], argv[0]
          );
        return 0;
      case 'v':
      case 'V':
        printf("%d\n", 4);
        return 0;
      case 'S':
      {
        int ok = 1;
        ok &= write_file("harry.c", _binary_harry_c_start, _binary_harry_c_end);
        ok &= write_file("Makefile", _binary_Makefile_start, _binary_Makefile_end);
        ok &= write_file("README.md", _binary_README_md_start, _binary_README_md_end);
        ok &= write_file("COPYING.md", _binary_COPYING_md_start, _binary_COPYING_md_end);
        return ! ok;
      }
    }
  }
  if (optind >= argc)
  {
    fprintf(stderr, "%s: error: missing argument\n", argv[0]);
    return 1;
  }
  int row = 0, col = 0;
  initscr();
  atexit(exit_cb);
  cbreak();
  keypad(stdscr, TRUE);
  noecho();
  curs_set(0);
  const char *msg = "...loading...";
  getmaxyx(stdscr, row, col);
  mvprintw(row / 2, (col -  strlen(msg)) / 2, "%s", msg);
  refresh();
  struct waveform_list *l =
    build_mipmaps(singleton(read_sound_file(argv[optind])));
  if (! l)
  {
    endwin();
    fprintf(stderr, "%s: error: failed to load '%s'\n", argv[0], argv[optind]);
    return 1;
  }
  struct audio audio = { l, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 };
  int audio_running = 0;
#ifdef WITH_SDL2
  SDL_AudioDeviceID dev = 0;
  if (adev == audio_sdl)
  {
    SDL_Init(SDL_INIT_AUDIO);
    SDL_AudioSpec want, have;
    want.freq = l->tail.prev->samplerate;
    want.format = AUDIO_F32;
    want.channels = l->tail.prev->channels;
    want.samples = 4096;
    want.callback = audio_cb;
    want.userdata = &audio;
    dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_ANY_CHANGE);
    if (dev)
    {
      if (have.format != AUDIO_F32 || have.channels != want.channels)
      {
        endwin();
        fprintf(stderr, "%s: error: bad audio parameters\n", argv[0]);
        fprintf
          ( stderr, "want: %d %d %d %d\n"
          , want.freq, want.format, want.channels, want.samples
          );
        fprintf
          ( stderr, "have: %d %d %d %d\n"
          , have.freq, have.format, have.channels, have.samples
          );
      }
      else
      {
        audio.factor = have.freq / l->tail.prev->samplerate;
        SDL_PauseAudioDevice(dev, 0);
        audio_running = 1;
      }
    }
  }
#endif
  int display_mode = 0;
  struct cursor *c = cursor_init(l, col, display_mode);
  int running = TRUE;
  struct timespec last, now;
  while (running)
  {
    erase();
    (display_mode ? display_spectrum : display_waveform)
      (c, argv[optind], &audio);
    refresh();
    getmaxyx(stdscr, row, col);
    switch (getch())
    {
      case KEY_UP: cursor_up(c, col, display_mode); break;
      case KEY_DOWN: cursor_down(c, col); break;
      case KEY_LEFT:
        cursor_left(c, col);
        if (audio.follow)
          audio.offset = c->offset / c->wave->samplerate * l->tail.prev->samplerate;
        break;
      case KEY_RIGHT:
        cursor_right(c, col);
        if (audio.follow)
          audio.offset = c->offset / c->wave->samplerate * l->tail.prev->samplerate;
        break;
      case KEY_HOME:
        cursor_home(c, col, display_mode);
        if (audio.follow)
          audio.offset = c->offset / c->wave->samplerate * l->tail.prev->samplerate;
        break;
      case KEY_END:
        cursor_end(c, col, display_mode);
        if (audio.follow)
          audio.offset = c->offset / c->wave->samplerate * l->tail.prev->samplerate;
        break;
      case ' ':
      case 'P':
      case 'p':
        audio.playing = ! audio.playing;
        if (audio.playing) {
          halfdelay(1);
          clock_gettime(CLOCK_MONOTONIC, &last);
        }
        audio_speed(&audio, audio.playing * pow(FACTOR, c->logspeed));
        break;
      case 'w':
      case 'W':
        display_mode = 0;
        break;
      case 's':
      case 'S':
        while (! c->wave->spectrum && cursor_down(c, col))
          ;
        display_mode = 1;
        break;
      case 'F':
      case 'f': audio.follow = ! audio.follow; break;
      case 'L': audio.looping = ! audio.looping; break;
      case 'l':
        switch (audio.ab)
        {
          case 0: audio.loop_start = audio.offset; audio.ab = 1; break;
          case 1: audio.loop_end = audio.offset; audio.ab = 2; break;
          case 2: audio.ab = 0; break;
        }
        break;
      case '[':
        c->logspeed -= 1;
        audio_speed(&audio, audio.playing * pow(FACTOR, c->logspeed));
        break;
      case ']':
        c->logspeed += 1;
        audio_speed(&audio, audio.playing * pow(FACTOR, c->logspeed));
        break;
      case 10:
        c->logspeed = 0;
        audio_speed(&audio, audio.playing * pow(FACTOR, c->logspeed));
        break;
      case '9':
        audio.loggain -= 1;
        audio.gain = pow(FACTOR, audio.loggain);
        break;
      case '0':
        audio.loggain += 1;
        audio.gain = pow(FACTOR, audio.loggain);
        break;
      case 'M': case 'm': audio.mute = ! audio.mute; break;
      case '+': case '=': c->loggain += 1; break;
      case '-': c->loggain -= 1; break;
      case '1': c->loggain = 0; break;
      case 27: case 'Q': case 'q': running = FALSE; break;
    }
    if (audio.playing)
    {
      if (audio_running)
      {
        if (audio.follow)
        {
          double offset = audio.offset;
          c->offset = offset / l->tail.prev->samplerate * c->wave->samplerate;
        }
      }
      else
      {
        clock_gettime(CLOCK_MONOTONIC, &now);
        double dt = (now.tv_sec - last.tv_sec)
                   + (now.tv_nsec - last.tv_nsec) / 1.0e9;
        last.tv_sec = now.tv_sec;
        last.tv_nsec = now.tv_nsec;
        c->offset += pow(FACTOR, c->logspeed) * c->wave->samplerate * dt;
        if (c->offset >= c->wave->frames)
        {
          if (audio.looping)
          {
            c->offset = fmod(c->offset, c->wave->frames);
          }
          else
          {
            c->offset = c->wave->frames;
            audio.playing = FALSE;
            nocbreak();
            cbreak();
            audio_speed(&audio, audio.playing * pow(FACTOR, c->logspeed));
          }
        }
      }
    }
    else
    {
      nocbreak();
      cbreak();
    }
  }
#ifdef WITH_SDL2
  if (adev == audio_sdl)
  {
    SDL_CloseAudioDevice(dev);
    SDL_Quit();
  }
#endif
  free_waveform_list(l);
  free(c);
  return 0;
}
